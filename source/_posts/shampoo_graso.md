---
title: Shampoo Sólido, Cabello Graso
cover: /img/shampoo_graso.jpg
price: 410
date: "2020-10-02"
author:
 - name: Loop
   link: https://instagram.com/loop_cosmetica?igshid=b8u8sdihojou
---
Este shampoo se recomienda para cueros cabelludos que produzcan mucho sebo y cualquier tipo de
porosidad. Apto para rutinas de vida agitada. Este shampoo puede resecar una vez que el cuero
cabelludo se equilibra, por lo que recomienda intercalar con otra variedad, de ser necesario.

Ingredientes:
- SCI (sodium cocoil isethionate): Tensoactivo (agente limpiador) derivado del coco, adecuado para
cabello y piel. Biodegradable. Soluble en agua y gran generador de espuma. Los productos
elaborados con SCI suavizan y acondicionan el cabello y la piel.
- Betaina de coco: Tensiactivo (agente limpiador) derivado del coco. Este tensiactivo es mucho más
suave que la mayoría y muy gentil con la piel y el cabello.
- Arcilla verde (bentonita): Un tipo de arcilla rica en oligoelementos y sales minerales. Debido a sus
propiedades como adsorbente de grasas y proteinas, resulta adecuada para la limpieza y eliminación
de impurezas de cueros cabelludos con una excesiva producción de cebo.
- Aceite de jojoba: La jojoba es originaria del desierto de Sonora (frontera entre Méjico y Estados
Unidos). Esta planta produce un aceite (que en realidad es una cera líquida) muy similar al sebo
natural que produce nuestra piel. Contiene gran cantidad de vitamina E (antioxidante). Retiene la
humedad, sella la cutícula capilar y ayuda a equilibrar la producción de sebo.
- Aceite esencial de limón: Obtenido de la cáscara del limón. Posee propiedades astringentes y
antioxidantes.
