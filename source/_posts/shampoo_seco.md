---
title: Shampoo Sólido, Cabello Seco
cover: /img/shampoo_seco.jpg
price: 410
date: "2020-10-01"
author:
 - name: Loop
   link: https://instagram.com/loop_cosmetica?igshid=b8u8sdihojou
---

Este shampoo se recomienda para cueros cabelludos y cabellos secos y cualquier tipo de porosidad.
Apto para cabellos finos. Puede utilizarse diariamente. Apto para rutinas de vida agitada.

Ingredientes:
- SCI (sodium cocoil isethionate): Tensoactivo (agente limpiador) derivado del coco, adecuado para
cabello y piel. Biodegradable. Soluble en agua y gran generador de espuma. Los productos
elaborados con SCI suavizan y acondicionan el cabello y la piel.
- Betaina de coco: Tensiactivo (agente limpiador) derivado del coco. Este tensiactivo es mucho más
suave que la mayoría y muy gentil con la piel y el cabello.
- Arcilla blanca (caolín): Un tipo de arcilla de gran pureza. Debido a sus propiedades como
adsorbente de grasas y proteinas, resulta adecuada para la limpieza y eliminación de impurezas de
cueros cabelludos pero no resulta tan astringente como la bentonita, lo que la vuelve adecuada para
pieles secas y sensibles.
- Infusión de cola de caballo: Originaria de Europa, posee una gran cantidad de oligoelementos,
flavonoides y taninos. Ayudan a fortalecer la hebra capilar.
- Manteca de cacao: Manteca vegetal obtenida de las semillas del cacao. Gran humectante (retiene
la hidratación) y emoliente (suaviza la superficie) brinda elasticidad a la piel y el cabello. Rica en
vitamina E y antioxidantes.
- Aceite de jojoba: La jojoba es originaria del desierto de Sonora (frontera entre Méjico y Estados
Unidos). Esta planta produce un aceite (que en realidad es una cera líquida) muy similar al sebo
natural que produce nuestra piel. Contiene gran cantidad de vitamina E (antioxidante). Retiene la
humedad, sella la cutícula capilar y ayuda a equilibrar la producción de sebo.
