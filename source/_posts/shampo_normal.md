---
title: Shampoo Sólido, Cabello Normal
cover: /img/product1.jpg
price: 410
date: "2020-10-03"
author:
 - name: Loop
   link: https://instagram.com/loop_cosmetica?igshid=b8u8sdihojou
---

Este shampoo se recomienda para cueros cabelludos equilibrados, cabellos de porosidades medias o media/alta, de grosor medio a alto. Apto para una elevada frecuencia de lavados y rutinas agitadas de vida.

Ingredientes:
- SCI (sodium cocoil isethionate): Tensoactivo (agente limpiador) derivado del coco, adecuado para
cabello y piel. Biodegradable. Soluble en agua y gran generador de espuma. Los productos
elaborados con SCI suavizan y acondicionan el cabello y la piel.
- Betaina de coco: Tensiactivo (agente limpiador) derivado del coco. Este tensiactivo es mucho más
suave que la mayoría y muy gentil con la piel y el cabello.
- Manteca de karité: Manteca vegetal obtenida de las semillas del árbol de karité, de origen africano.
Humectante (retiene la hidratación) y emoliente (suaviza la superficie) brinda elasticidad a la piel y
el cabello. Rica en vitaminas A, D, E y F. Cicatrizante. La recolección de nueces y obtención de ste
llamado “oro Africano” da trabajo a miles de mujeres en África occidental.
- Aceite de coco vírgen: Aceite vegetal obtenido del fruto del coco. Humectante (retiene la
hidratación) y emoliente (suaviza la superficie) brinda elasticidad al cabello. Es el aceite de mayor
penetración en la hebra capilar.
- Aceite de jojoba: La jojoba es originaria del desierto de Sonora (frontera entre Méjico y Estados
Unidos). Esta planta produce un aceite (que en realidad es una cera líquida) muy similar al sebo
natural que produce nuestra piel. Contiene gran cantidad de vitamina E (antioxidante). Retiene la
humedad, sella la cutícula capilar y ayuda a equilibrar la producción de sebo.
- Extracto de vainilla: Provee un aroma agradable para la expriencia en la ducha.
